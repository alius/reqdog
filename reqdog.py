#!/usr/bin/env python

'''
Copyright (c) 2014, Conversocial Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 3. Neither the name of the copyright holder nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
'''

__author__ = 'Artiom Vasiliev'
__version__ = '0.1'

'''
This is a very basic checker for requirements.txt to see which packages are
outdated and need to be updated.
'''

import os
import sys
import subprocess
from optparse import OptionParser


class Colors:
    RED = '\033[31m'
    GREEN = '\033[32m'
    YELLOW = '\033[33m'
    BLUE = '\033[34m'
    PURPLE = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'

    INFO = CYAN
    WARNING = YELLOW
    ERROR = RED
    ENDC = '\033[0m'


class StdOut:
    @staticmethod
    def info(message):
        print Colors.INFO + '[*] ' + message + Colors.ENDC

    @staticmethod
    def error(message):
        print Colors.ERROR + '[!] ' + message + Colors.ENDC

    @staticmethod
    def warning(message):
        print Colors.WARNING + '[!] ' + message + Colors.ENDC


def generate_report():
    outdated = 0
    updated = 0
    for dependency, data in requirements.iteritems():
        if len(data) > 1:
            StdOut.info('%s is specified in multiple files:' % dependency)
            for path in data.keys():
                StdOut.info(' > %s' % path)

        is_outdated = True
        is_different_version = False
        required_version = None
        for path, versions in data.iteritems():
            is_outdated = is_outdated and versions['outdated']
            if not required_version:
                required_version = versions['installed']
            else:
                if required_version != versions['installed']:
                    is_different_version = True

        if is_different_version:
            StdOut.info('Different versions of "%s" are required in the '
                        ' following files: %s' %
                        (dependency, ', '.join(data.keys())))

        if is_outdated:
            outdated += 1
        else:
            updated += 1

    print ''
    StdOut.info('Outdated packages: %s' % outdated)
    StdOut.info('Up-to-date packages: %s' % updated)


def _log_dependency(dependency, installed_v, latest_v, source):
    ''' Builds a dictionary with all the dependencies, their current
    and latest versions in order to do a report
    '''
    if dependency in requirements:
        if source not in requirements[dependency]:
            requirements[dependency][source] = {
                'installed': installed_v, 'latest': latest_v,
                'outdated': installed_v != latest_v}
    else:
        requirements[dependency] = {source: {
            'installed': installed_v, 'latest': latest_v,
            'outdated': installed_v != latest_v}}


def _get_reqs_from_file(file_path):
    ''' Generator that returns line at a time
    '''
    if not os.path.isfile(file_path):
        StdOut.error('Cannot open %s' % file_path)
        return

    with open(file_path, 'r') as f:
        for line in f:
            yield line


def _parse_version(haystack, needle):
    ''' Given the string and the text to search for, returns the version.
    For example from the string "INSTALLED: 1.2.3" returns "1.2.3"
    '''
    start = haystack.index(needle)
    stop = haystack.index('\n', start)
    return haystack[start:stop].split(needle)[1].strip(' \n')


def _check_local(req_path):
    global reqs_home
    if req_path[0] != '-':
        return

    reqs = os.path.abspath(reqs_home + '/' + req_path[3:].strip(' \n'))
    check_requirements(reqs)


def _check_external(req, req_source):
    if '==' not in req:
        return

    dependency, _ = req.split('==')
    p = subprocess.Popen(['pip', 'search', dependency], stdout=subprocess.PIPE)
    out, err = p.communicate()
    if out:
        try:
            installed_v = _parse_version(out, 'INSTALLED:')
            latest_v = _parse_version(out, 'LATEST:')
        except ValueError:
            _log_dependency(dependency, 'x', 'y', req_source)
            StdOut.error('Failed to check %s' % dependency)
            return

        _log_dependency(dependency, installed_v, latest_v, req_source)
        if installed_v != latest_v:
            msg = '%s@v%s is outdated, the latest version is %s' % \
                (dependency, installed_v, latest_v)
            StdOut.warning(msg)


def check_requirements(req_path):
    for requirement in _get_reqs_from_file(req_path):
        if requirement:
            if requirement[:1] in ['.', '-']:
                _check_local(requirement)
            else:
                _check_external(requirement, req_path)


parser = OptionParser()
parser.add_option('-f', '--requirements', dest='main_reqs',
                  help='Path to the main requirements file')
parser.add_option('-d', '--home', dest='reqs_home',
                  help='Full path to the project home directory')

options, _ = parser.parse_args()

if not options.main_reqs:
    StdOut.error('Path to the main requirements file is missing')
    parser.print_help()
    sys.exit()
if not options.reqs_home:
    StdOut.error('Full path to the project home directory is missing')
    parser.print_help()
    sys.exit()

reqs_home = options.reqs_home
main_reqs = options.main_reqs
requirements = {}

if not os.path.isfile(main_reqs):
    StdOut.error('Cannot open %s' % main_reqs)
    sys.exit()

check_requirements(main_reqs)
generate_report()
